process.env.NODE_ENV = 'test';
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const server = require('../src/server');
const fakeData = require('../src/database/fake_db');

chai.use(chaiHttp);

describe('Have to get User token after all request', () => {

    let token = null;

    describe('/POST login', () => {
        it('it should login user', (done) => {
            chai.request(server)
                .post('/login')
                .send({
                    email: 'johndoe@domain.ext',
                    password: '123456',
                })
                .end((err, res) => {
                    token = res.body.token;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });
    });

    describe("/GET get all rooms", () => {
        it("it should get all rooms", (done) => {
            chai.request(server)
            .get('/rooms')
            .set('Authorization', `Bearer ${token}`)
            .end((err, res) => {
                res.should.have.status(200);
            });
        });
    });
});
