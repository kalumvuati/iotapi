require("dotenv").config();
const jwt = require("jsonwebtoken");
const env = process.env.NODE_ENV || 'development';
const {config} = require('../../config/config.js');
const config_env = config[env];

exports.isAuthenticated = (req, rep, next) => {
    let token = req.headers?.authorization?.split(' ')[1];
    let data = {
        message: "Access denied",
        data: null,
        status: 401
    };

    try {
        if ((token && jwt.verify(token, config_env.SESSION_SECRET)) ||
            jwt.verify(req.cookies.token, config_env.SESSION_SECRET)) {
            next();
        }
    } catch (err) {
        rep.status(data.status).send(data)
    }
}

exports.cleanCookie = (request, response, message) => {
    let data = {
        message: message || "User not logged in",
        data: null,
        status: 401
    };
    try {
        //Remove cookie token from response
        if (request.cookies.token) {
            data.message = 'You have been logged out';
            data.status = 200; // Lunching the response
            return response.status(data.status).clearCookie('token').send(data);
        } else {
            data.message = 'User not logged in';
        }
    } catch (e) {
        data.message = 'No cookies found'
    }

    response.status(data.status).send(data);
}
