const express = require("express");
const router = express.Router();
const roomController  = require("../controllers/roomController");

/**
 * @swagger
 * components:
 *  schemas:
 *   Room:
 *    type : object
 *    properties :
 *     roomId :
 *      type : string
 *      description : The auto-generated id of Room
 *      example : "HPB/ETG.01/SAL.045"
 *     humidity :
 *      type : object
 *      description : The humidity of Room
 *      example: { value: 23, unit: "%", updated_at: "2022-07-29T00:00:00.000Z" }
 *     temperature :
 *      type : object
 *      description : The temperature of Room
 *      example: { value: 28.5, unit: "°c", updated_at: "2022-07-29T00:00:00.000Z" }
 *     positions :
 *      type : array
 *      description : The positions of sensors in Room
 *      example: [{ tag_rssi: -80, tag_address: "24:62:AB:BA:40:56", tag_name: "HPB#BRD.14-ETG.01-SAL.045", updated_at: "2022-07-29T00:00:00.000Z" }]
 *   Rooms:
 *    type : object
 *    properties :
 *     message :
 *      type : string
 *      description : The message of the response
 *      example : "Rooms found"
 *     data :
 *      type : array
 *      description : The list of Rooms
 *      example : [{ roomId: "HPB/ETG.01/SAL.045", humidity: { value: 23, unit: "%", updated_at: "2022-07-29T00:00:00.000Z"  }, temperature: { value: 28.5, unit: "°c", updated_at: "2022-07-29T00:00:00.000Z" }, positions: [{ tag_rssi: -80, tag_address: "24:62:AB:BA:40:56", tag_name: "HPB#BRD.14-ETG.01-SAL.045", updated_at: "2022-07-29T00:00:00.000Z" }] }]
 *     status :
 *      type : integer
 *      description : The status of the response
 *      example : 200
 */

/**
 * @swagger
 * /rooms :
 *  get :
 *    tags : [Rooms]
 *    description : "Return all rooms"
 *    responses :
 *     '200' :
 *       description : A successful response
 *       content :
 *        application/json :
 *         schema :
 *          $ref : '#/components/schemas/Rooms'
 */
router.route("/")
    .get(roomController.getRooms);

/**
 * @swagger
 * /rooms/{id}:
 *  get:
 *   tags: [Rooms]
 *   summary: "Return the Room  with the given id"
 *   description: "Return the Room  with the given id"
 *   parameters:
 *    - in: path
 *      name: id
 *      schema:
 *       type: string
 *      description: The id of the Room 
 *      required: true
 *      example: 'HPB/ETG.01/SAL.045'
 *   responses:
 *    200:
 *     description: Room  found
 *     content:
 *      application/json:
 *       schema:
 *        $ref: '#/components/schemas/Room'
 *        description: The Room  with the given id
 *        example:
 *         roomId: "HPB/ETG.01/SAL.045"
 *         humidity: { value: 23, unit: "%", updated_at: "2022-07-29T00:00:00.000Z"  }
 *         temperature: { value: 28.5, unit: "°c", updated_at: "2022-07-29T00:00:00.000Z" }
 *         positions: [{ tag_rssi: -80, tag_address: "24:62:AB:BA:40:56", tag_name: "HPB#BRD.14-ETG.01-SAL.045", updated_at: "2022-07-29T00:00:00.000Z" }]
 *         status: 200
 *    401:
 *     description: Unauthorized
 *     content:
 *      application/json:
 *       schema:
 *        properties:
 *         message:
 *          type: string
 *          description: The error message
 *          example: "Unauthorized"
 *    404:
 *     description: "No Room found"
 *     content:
 *      application/json:
 *       schema:
 *        properties:
 *         message:
 *          type: string
 *          description: The error message
 *          example: "No Room found"
 */
router.route("/:id")
    .get(roomController.getRoom);

module.exports = router;
