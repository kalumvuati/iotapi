const express = require("express");
const router = express.Router();
const secureController  = require("../controllers/secureController");

router.route("/")
    .get(secureController.home)

/**
 * @swagger
 * /login:
 *  post:
 *   tags: [Login]
 *   description: "Return user token"
 *   requestBody:
 *    content:
 *     application/json:
 *      schema:
 *       properties:
 *        email :
 *         type : string
 *         description : The email of the User
 *         example: "johndoe@domain.ext"
 *         required: true
 *        password :
 *         type : string
 *         description : The password of the User
 *         example: "123456"
 *         required: true
 *   responses:
 *    '200':
 *      description: A successful response
 *      content:
 *       application/json:
 *        schema:
 *         properties:
 *          token :
 *           type : string
 *           description : The token of the User
 *           example: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImpvaG5kb2VAZG9tYWluLmV4dCIsImlhdCI6MTY1NzI2OTAzNywiZXhwIjoxNjU3Mjc5ODM3fQ.jjHwSwqoGLbDLXuLP5pfJ64xa_P4WCXea6iJErEuAWE"
 *
 *    '202':
 *      description: User is already logged
 *      content:
 *       application/json:
 *        schema:
 *         properties:
 *          email :
 *           type : string
 *           description : The email of the User
 *           example: "johndoe@domain.ext"
 *          iat :
 *           type : string
 *           description : The email of the User
 *           example: "1657269309"
 *          exp :
 *           type : string
 *           description : The email of the User
 *           example: "1657280109"
 *    '400':
 *     description: Bad request
 *     content:
 *      application/json:
 *       schema:
 *        properties:
 *         message :
 *          type : string
 *          description : The error message
 *          example: "Invalid email or password"
 *    '404':
 *     description: Not found
 *     content:
 *      application/json:
 *       schema:
 *        properties:
 *         message :
 *          type : string
 *          description : The error message
 *          example: "User not found"
 */
router.route("/login")
    .post(secureController.login)

module.exports = router;
