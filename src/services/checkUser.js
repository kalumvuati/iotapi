require("dotenv").config();
const jwt = require("jsonwebtoken");
const env = process.env.NODE_ENV || 'development';
const {config} = require('../../config/config.js');
const config_env = config[env];

exports.checkUser = (req) => {
    // Checking Bearer
    let token = req.headers?.authorization?.split(' ')[1];
    try {
        // login by form
        return (token && jwt.verify(token, config_env.SESSION_SECRET)) ||
            jwt.verify(req.cookies.token, config_env.SESSION_SECRET);
    } catch (err) {
        return false;
    }
}
