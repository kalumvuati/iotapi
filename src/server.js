// Importing libraries
const cookieParser = require('cookie-parser');
const cors = require('cors');
const  express = require("express");
const morgan = require('morgan');
const swaggerUi  = require('swagger-ui-express');
const swaggerJsDOc  = require('swagger-jsdoc');
const mqtt = require('mqtt'); // A library for working with MQTT protocol

require('dotenv').config();
const env = process.env.NODE_ENV || 'development';
const {config} = require('../config/config.js');
const config_env = config[env];

// Local SGBD
// We don't need to use a database for this project
const fakeData = require("./database/fake_db");

const SENSORS = {
    TEMPERATURE: "temperature",
    HUMIDITY: "humidity",
    LIGHT: "light",
    MOTION: "motion",
    BLUETOOTH: "ble-devices",
}

//
const topicList = ["HPB/#"];

const app = express();
app.use(require('body-parser').json());
const port = config_env.EXTERNAL_PORT || 3000;

// Swagger - Doc ----------------------------------------------------------
const swaggerDefinition = {
    openapi: '3.0.0',
    info: {
        title: "IOT API",
        version: "1.0.0",
        description: "API for IOT",
        contact: {
            email: 'duramana.kalumvuati@epitehc.eu',
            name: 'Duramana et Aimé ',
            url: 'https://github.com/EpitechMscProPromo2023/T-MAJ-800-TEAM_2/tree/develop/iot',
        }
    },
    host: config_env.SITE_URL,
    basePath:"/",
    schemes: ['http', 'https'],
    consumes: ['application/json'],
    components: {
        securitySchemes: {
            bearerAuth: {
                type: 'http',
                scheme: 'bearer',
                bearerFormat: 'JWT',
            }
        }
    },
    security: [{
        bearerAuth: []
    }],
    logging: false,
};
const options = {
    swaggerDefinition,
    apis: ["src/router/*.js"],
};
const swagger_Specs = swaggerJsDOc(options);
const options_css = {
    customCss: '.swagger-ui .topbar { display: none }'
};
//-------------------------------------------------------------------------

// Routes
const roomRoutes = require("./router/roomRoutes");
const secureRoutes = require("./router/secureRoutes");
const isAuthenticated = require("./middleware/currentUser");

// Configuration of the Request
app.use((req, res , next )=>{
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods',['GET', 'POST']);
    next();
});

// MIDDLEWARES ------------------------------------------------------------
app.use(morgan('dev'));
app.use(cookieParser());
app.use(cors(
    {
        origin: "*",
        credentials: true,
        allowedHeaders: [
            'Content-Type',
            'Authorization',
            'Accept',
            'Origin',
            'X-Requested-With',
            'Access-Control-Allow-Origin',
            'Access-Control-Allow-Headers',
            'Access-Control-Allow-Methods',
            'Access-Control-Allow-Credentials'
        ],
        methods: ['GET', 'POST'],
    }
));

// TODO :  to finish this function
// app.use(session({
//     secret: config_env.SESSION_SECRET,
//     resave: false,
//     saveUninitialized: true,
//     cookie: {
//         secure: true
//     }
// }));
//-------------------------------------------------------------------------

// Setting up the mqtt client
const mqttClient = mqtt.connect(`${config_env.MQTT_PROTOCOL}://${config_env.MQTT_SERVER}`, {
    port: config_env.MQTT_PORT,
    username: config_env.MQTT_USERNAME,
    password: config_env.MQTT_PASSWORD
});

// Checking the successful connection to the broker
mqttClient.on("connect", () => {
    // Confirmation message when successful connection to the broker
    console.log("connected to the broker\n\n");

    // Subscribe to all topics to be used
    mqttClient.subscribe(topicList, {qos:2}, function (error){
        if (error) {
            console.log("Error when subscribing topics. Here is some details: " + error.message)
        }
    })
});

// Checking for error when connection to the broker is unsuccessful
mqttClient.on('error', function (error) {
    console.log("Error when connecting to the broker. Here is some details: " + error.message);
});

// Subscribing to the broker topic and updating trashes status to the API
mqttClient.on('message', async (topic, message) => {

    let found = false;
    let dataFromArduino = '';

    if (topic === `HPB/ETG.01/SAL.045/${SENSORS.HUMIDITY}`) {

        dataFromArduino = JSON.parse(message.toString());
        fakeData[0].data.map(room => {
            if (room.roomId === dataFromArduino.roomId) {
                if ( room.humidity.value !== dataFromArduino.humidity) {
                    room.humidity.value = dataFromArduino.humidity;
                    room.humidity.unit = dataFromArduino.unit; // useless
                    room.humidity.updated_at = Date.now();
                }
                found = true;
            }
        })

        // Register a new Room
        if (!found) {
            fakeData[0].data.push({
                roomId: dataFromArduino.roomId,
                humidity: {
                    value: dataFromArduino.humidity,
                    unit: dataFromArduino.unit,
                    updated_at: Date.now()
                },
                temperature: {},
                positions: []
            });
        }
    }

    if (topic === `HPB/ETG.01/SAL.045/${SENSORS.TEMPERATURE}`) {

        dataFromArduino = JSON.parse(message.toString());
        found = false; // useless

        fakeData[0].data.map(room => {
            if (room.roomId === dataFromArduino.roomId) {
                if ( room.temperature.value !== dataFromArduino.temperature) {
                    room.temperature.value = dataFromArduino.temperature;
                    room.temperature.unit = dataFromArduino.unit; // useless
                    room.temperature.updated_at = Date.now();
                }
                found = true;
            }
        })

        // Register a new Room
        if (!found) {
            fakeData[0].data.push({
                roomId: dataFromArduino.roomId,
                temperature: {
                    value: dataFromArduino.temperature,
                    unit: dataFromArduino.unit,
                    updated_at: Date.now()
                },
                humidity: {},
                positions: []
            });
        }
    }

    if (topic === `HPB/ETG.01/SAL.045/${SENSORS.BLUETOOTH}`) {

        dataFromArduino = JSON.parse(message.toString());
        found = false; // useless

        fakeData[0].data.map(room => {
            if (room.roomId === "HPB/ETG.01/SAL.045") {
                room.positions.map(position => {
                    if (position.tag_rssi !== dataFromArduino[dataFromArduino.length - 1].tag_rssi) {
                        position.tag_rssi = dataFromArduino[dataFromArduino.length - 1].tag_rssi;
                        position.tag_address = dataFromArduino[dataFromArduino.length - 1].tag_address; // useless
                        position.tag_name = dataFromArduino[dataFromArduino.length - 1].tag_name; // useless
                        position.updated_at = Date.now();
                    }
                })
                found = true;
            }
        })

        // Register a new Room
        if (!found) {
            fakeData[0].data.push({
                roomId: "HPB/ETG.01/SAL.045",
                positions: [
                    {
                        tag_rssi: dataFromArduino[dataFromArduino.length - 1].tag_rssi,
                        tag_address: dataFromArduino[dataFromArduino.length - 1].tag_address,
                        tag_name: dataFromArduino[dataFromArduino.length - 1].tag_name,
                        updated_at: Date.now()
                    }
                ],
                humidity: {},
                temperature: {}
            });
        }
    }
});

// Routes - Middleware
app.use("/", secureRoutes);
app.use("/rooms", isAuthenticated.isAuthenticated, roomRoutes);

// Swagger - Doc
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swagger_Specs, options_css));
module.exports = app.listen(port, () => {
    console.log(`Listening on Server port : ${port}`);
});

module.exports = app;


