const { checkUser } = require("../services/checkUser");
const fakeData = require("../database/fake_db");

exports.getRooms = async function (request, response) {
    let data = {
        message: "No Room found",
        data: null,
        status: 404
    }

    const user_from_cookies = checkUser(request);
    if (user_from_cookies) {
        data.data = fakeData[0].data;
        if (data.data.length > 0) {
            data.status = 200;
            data.message = "Rooms found";
        }
    }

    return response.status(data.status).json(data);
}

exports.getRoom = async function (request, response) {
    let data = {
        message: "Room not found",
        data: null,
        status: 404
    }

    const user_from_cookies = checkUser(request);
    if (user_from_cookies) {
        const room_id = request.params.id;
        const rooms = fakeData[0].data.filter((room ) =>{
            return room.roomId == room_id ? room : null;
        });
        if (rooms.length > 0) {
            data.data = rooms[0];
            data.message = "Room found";
            data.status = 200;
        }
    }

    return response.status(data.status).json(data);
}
