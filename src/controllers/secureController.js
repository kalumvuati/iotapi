const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { checkUser } = require("../services/checkUser");
require("dotenv").config();
const env = process.env.NODE_ENV || 'development';
const {config} = require('../../config/config.js');
const config_env = config[env];
const fakeData = require("../database/fake_db");

exports.login = async function (request, response) {

    let data = {
        message: "Invalid email or password. Check you log before to retry.",
        token: null,
        status: 401
    }

    const { email, password } = request.body;
    if (!email || !password) {
        return response.status(401).json(data);
    }

    const user = await checkUser(request);

    if (!user) {
        const user_found = fakeData[fakeData.length - 1].users.find(user => user.email === email);
        const isValidPassword = await bcrypt.compare(password, user_found.password);

        if (isValidPassword) {
            data.token = jwt.sign(
                    {
                        id: user.id,
                        email,
                    },
                    config_env.SESSION_SECRET,
                    {expiresIn: '3h'}
                );
                data.message = "Login successful";
                data.status = 200;
            return response.status(data.status).cookie('token', data.token).json(data);
        }
    } else if (user.email === email) {
        return response.status(202).json(user);
    }

    return response.status(401).json(data);
}

exports.home = async function (request, response) {
    return response.redirect('/api-docs');
}