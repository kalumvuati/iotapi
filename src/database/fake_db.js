// Rooms
module.exports = [
    {
        data : [

            {
                roomId: "HPB/ETG.01/SAL.045",
                humidity: {
                    value: 23,
                    unit: "%",
                    updated_at: "2022-07-29T00:00:00.000Z"
                },
                temperature: {
                    value: 28.5,
                    unit: "°c",
                    updated_at: "2022-07-29T00:00:00.000Z"
                },
                positions: [
                    {
                        tag_rssi: -80,
                        tag_address: "24:62:AB:BA:40:56",
                        tag_name: "HPB#BRD.14-ETG.01-SAL.045",
                        updated_at: "2022-07-29T00:00:00.000Z"
                    }
                ]
            },
            {
                roomId: "HPB/ETG.01/SAL.046",
                humidity: {
                    value: 20,
                    unit: "%",
                    updated_at: "2022-07-29T00:00:00.000Z"
                },
                temperature: {
                    value: 31.5,
                    unit: "°c",
                    updated_at: "2022-07-29T00:00:00.000Z"
                },
                positions: [
                    {
                        tag_rssi: -30,
                        tag_address: "24:62:AB:BA:40:56",
                        tag_name: "HPB#BRD.14-ETG.01-SAL.045",
                        updated_at: "2022-07-29T00:00:00.000Z"
                    }
                ]
            },
            {
                roomId: "HPB/ETG.01/SAL.047",
                humidity: {
                    value: 32,
                    unit: "%",
                    updated_at: "2022-07-29T00:00:00.000Z"
                },
                temperature: {
                    value: 30.5,
                    unit: "°c",
                    updated_at: "2022-07-29T00:00:00.000Z"
                },
                positions: [
                    {
                        tag_rssi: -50,
                        tag_address: "24:62:AB:BA:40:56",
                        tag_name: "HPB#BRD.14-ETG.01-SAL.045",
                        updated_at: "2022-07-29T00:00:00.000Z"
                    }
                ]
            }

        ]
    },
    {
        users: [
            {
                id: 1,
                email: 'johndoe@domain.ext',
                password: '$2a$10$C49Sk7q0wzu2QAYshEl3AO4TOxNE17C5k5rHI0Gh3oD9jyuNL3u0e',
            }
        ],
    }
];

