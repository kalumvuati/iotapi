require('dotenv').config();
exports.config  = {
    "development": {

        "SESSION_SECRET" : process.env.SESSION_SECRET,
        "SITE_URL": process.env.SITE_URL,
        "NODE_ENV": process.env.NODE_ENV,

        // MAILLING
        "AUTH_EMAIL" : process.env.AUTH_EMAIL,
        "AUTH_PASS" : process.env.AUTH_PASS,
        "EMAIL_SERVICE" : process.env.EMAIL_SERVICE,

        // MQTT
        'MQTT_SERVER': process.env.MQTT_SERVER,
        'MQTT_PORT': process.env.MQTT_PORT,
        'MQTT_PROTOCOL': process.env.MQTT_PROTOCOL,
        'MQTT_USERNAME': process.env.MQTT_USERNAME,
        'MQTT_PASSWORD': process.env.MQTT_PASSWORD,
    },
    "production": {

        "SESSION_SECRET" : process.env.SESSION_SECRET,
        "SITE_URL": process.env.SITE_URL,

        // MAILLING
        "AUTH_EMAIL" : process.env.AUTH_EMAIL,
        "AUTH_PASS" : process.env.AUTH_PASS,
        "EMAIL_SERVICE" : process.env.EMAIL_SERVICE,

        // MQTT
        'MQTT_SERVER': process.env.MQTT_SERVER,
        'MQTT_PORT': process.env.MQTT_PORT,
        'MQTT_PROTOCOL': process.env.MQTT_PROTOCOL,
        'MQTT_USERNAME': process.env.MQTT_USERNAME,
        'MQTT_PASSWORD': process.env.MQTT_PASSWORD,
    },
    "test": {

        "SESSION_SECRET" : process.env.SESSION_SECRET,

        // MAILLING
        "AUTH_EMAIL" : process.env.AUTH_EMAIL,
        "AUTH_PASS" : process.env.AUTH_PASS,
        "EMAIL_SERVICE" : process.env.EMAIL_SERVICE,

        // MQTT
        'MQTT_SERVER': process.env.MQTT_SERVER,
        'MQTT_PORT': process.env.MQTT_PORT,
        'MQTT_PROTOCOL': process.env.MQTT_PROTOCOL,
        'MQTT_USERNAME': process.env.MQTT_USERNAME,
        'MQTT_PASSWORD': process.env.MQTT_PASSWORD,
        EXTERNAL_PORT: process.env.EXTERNAL_PORT_TEST,
        SITE_URL: process.env.SITE_URL_TEST,
    }
}
